﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CoinMover : MonoBehaviour
{
    public float moveSpeed = .05F;
    // Update is called once per frame
    void Update()
    {

        transform.position += Vector3.up * moveSpeed;
        if (transform.position.y >= 2)
        {

            moveSpeed *= -1;
        }
        if (transform.position.y <= -2)
        {

            moveSpeed *= -1;
        }

    }
}
