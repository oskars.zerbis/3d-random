﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Popup : MonoBehaviour {

	public GameObject LosePopup;
	public GameObject WinPopup;
	public Text FinalGold;

	void OnTriggerEnter(Collider other)
	{
		if (other.tag == "Enemy") {
			LosePopup.SetActive (true);
			Cursor.visible = true;
			Time.timeScale = 0;
		}
		if (other.tag == "Goal") {
			WinPopup.SetActive (true);
			Cursor.visible = true;
			FinalGold.text = "You collected " +
			GetComponent<Collection> ().gold +
			" gold!";
			Time.timeScale = 0;
		}
	}
}
